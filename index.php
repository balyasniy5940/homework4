<?php
/*
* 1) Написать программу, которая выводит простые числа, т.е. делящиеся без
остатка только на себя и на 1
*/
$number = 0;
for ($i = 1; $i <= 100; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        if ($i % $j == 0) {
            $number++;
        }
    }
    if ($number <= 2) {
        echo $i . "<br>";
    }
    $number = 0;
};

/*
*2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.
*/

$y = 0;
for ($j = 1; $j <= 100; $j++) {
    if ($j % 2 == 0) {
        $y++;
    }
}
echo "<p>In one hundred $y even numbers</p>";

/*
* 3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).
*/
$first = 0;
$second = 0;
$third = 0;
$fourth = 0;
$fifth = 0;
for ($i = 1; $i <= 100; $i++) {
    $randomNumber = rand(1, 5);
    if ($randomNumber == 1) {
        $first++;
    } elseif ($randomNumber == 2) {
        $second++;
    } elseif ($randomNumber == 3) {
        $third++;
    } elseif ($randomNumber == 4) {
        $fourth++;
    } elseif ($randomNumber == 5) {
        $fifth++;
    }
}

echo "<p>The number 1 was displayed  times $first</p>" . "<p>The number 2 was displayed  times $second</p>" . "<p>The number 3 was displayed  times $third</p>" . "<p>The number 4 was displayed  times $fourth</p>" . "<p>The number 5 was displayed  times $fifth</p>";
/*
* 4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.
*/

echo "<table>";
for ($i = 0; $i < 3; $i++) {
    echo "<tr>";
    for ($j = 0; $j < 5; $j++) {
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        echo "<td style='background-color:rgb($r,$g,$b)'>&nbps</td>";
    }
    echo "</tr>";
}
echo "</table>";
/*
* 1) В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).
*/

$month = rand(1, 12);
if ($month == 1 || $month == 2 || $month == 12) {
    echo "<p>This is winter</p>";
}
if ($month == 3 || $month == 4 || $month == 5) {
    echo "<p>This is spring</p>";
}
if ($month == 6 || $month == 7 || $month == 8) {
    echo "<p>This is summer</p>";
}
if ($month == 9 || $month == 10 || $month == 11) {
    echo "<p>This is autumn</p>";
}

/*
* 2) Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.
*/
$str = 'abcde';
if ($str[0] == 'a') {
    echo "<p>Да</p>";
} else {
    echo "<p>Нет</p>";
}
/*
* 3)Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.
*/
$str = '12345';
if ($str[0] == '1' || $str[0] == 2 || $str[0] == 3) {
    echo "<p>Да</p>";
} else {
    echo "<p>Нет</p>";
}
/*
* 4)Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else.
*/
$test = true;
echo $test == true ? 'Верно' : 'Неверно';
if ($test == true) {
    echo "<p>Верно</p>";
} else {
    echo "<p>Неверно</p>";
}
/*
* 5) Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку.
*/
$english = ['mon', 'tue', 'wen', 'thu', 'fri', 'sat', 'sun'];
$rus = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$lang = 'ru';
if ($lang == 'ru') {
    print_r($rus);
} else {
    print_r($english);
}
echo $lang == 'ru' ? print_r($rus) : print_r($english);
/*
* 6) В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.
*/
$clock = rand(0, 59);
if ($clock <= 15) {
    echo "<p>First quarter</p>";
} elseif ($clock > 15 && $clock <= 30) {
    echo "<p>Second quarter</p>";
} elseif ($clock > 30 && $clock <= 45) {
    echo "<p>Third quarter</p>";
} elseif ($clock > 45 && $clock <= 59) {
    echo "<p>Fourth quarter</p>";
} else {
    echo "<p>Try again</p>";
}
echo $clock <= 15 ? 'First quarter' :
    ($clock > 15 && $clock <= 30 ? 'Second quarter' :
        ($clock > 30 && $clock <= 45 ? 'Third quarter' :
            ($clock > 45 && $clock <= 59 ? 'Fourth quarter' : 'Try again')));
echo "<br>";
/*
* Все задания делаем
while
do while
for
foreach

не используй готовые функции

1. Дан массив
['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
Развернуть этот массив в обратном направлении
*/
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
$j = 0;
//FOR
for ($i; $arrPeople[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrPeopleRevers[] = $arrPeople[$j];
}
print_r($arrPeopleRevers);
echo "<br>";

//WHILE

while (isset($arrPeople[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrPeopleRevers[] = $arrPeople[$j];
    $j--;
}
print_r($arrPeopleRevers);
echo "<br>";

//Do while
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
$j = 0;
do {
    $j = $i;
    $i++;
} while (isset($arrPeople[$i]));
do {
    $arrPeopleRevers[] = $arrPeople[$j];
    $j--;
} while ($j >= 0);
print_r($arrPeopleRevers);
echo "<br>";

//Foreach
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
foreach ($arrPeople as $key => $value) {
    $i++;
}
foreach ($arrPeople as $key => $value) {
    $arrPeopleRevers[] = $arrPeople[$i - 1];
    $i--;
}
print_r($arrPeopleRevers);
echo "<br>";

/*2. Дан массив
[44, 12, 11, 7, 1, 99, 43, 5, 69]
Развернуть этот массив в обратном направлении
*/
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
//for
for ($i; $arrNumber[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrNumberRevers[] = $arrNumber[$j];
}
print_r($arrNumberRevers);
echo "<br>";

//while

while (isset($arrNumber[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrNumberRevers[] = $arrNumber[$j];
    $j--;
}
print_r($arrNumberRevers);
echo "<br>";

//do while
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
do {
    $j = $i;
    $i++;
} while (isset($arrNumber[$i]));
do {
    $arrNumberRevers[] = $arrNumber[$j];
    $j--;
} while ($j >= 0);
print_r($arrNumberRevers);
echo "<br>";

//foreach
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
$arrNumberRevers = [];
foreach ($arrNumber as $key => $value) {
    $i++;
}
foreach ($arrNumber as $key => $value) {
    $arrNumberRevers[] = $arrNumber[$i - 1];
    $i--;
}
print_r($arrNumberRevers);
echo "<br>";

/*
 * 3. Дана строка
    let str = 'Hi I am ALex'
    развенуть строку в обратном направлении.
 */

$str = 'Hi I am ALex';

//for
$i = 0;
$j = 0;
$arrLetter = [];
for ($i; $str[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrLetter[] = $str[$j];
}
print_r($arrLetter);
echo "<br>";

//foreach
$str = 'Hi I am ALex';
$i = 0;
$strRevers = [];
while (isset($str[$i])) {
    $strRevers [] = $str[$i];
    $i++;
}
$stringReverse = '';
foreach ($strRevers as $key => $value) {
    $i--;
    $stringReverse .= $strRevers[$i] . "<br>";
}
echo $stringReverse;
echo "<br>";

//while
$i = 0;
$j = 0;
$arrLetter = [];
while (isset($str[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrLetter[] = $str[$j];
    $j--;
}
print_r($arrLetter);
echo "<br>";

// do while
$i = 0;
$j = 0;
$arrLetter = [];
do {
    $j = $i;
    $i++;
} while (isset($str[$i]));
do {
    $arrLetter[] = $str[$j];
    $j--;
} while ($j >= 0);
print_r($arrLetter);
echo "<br>";

/*
 * 4. Дана строка. готовую функцию toUpperCase() or tolowercase()
let str = 'Hi I am ALex'
сделать ее с с маленьких букв
 */


echo mb_strtolower($str);
echo "<br>";
/* If i can't use it, then here :
*/

//for
$str = 'Hi I am ALex';
$strSmall = '';
$i = 0;
$j = 0;
for ($i; isset($str[$i]); $i++) {
    $j = $i;
}
for ($j = 0; $j <= $i; $j++) {
    $strSmall .= mb_strtolower($str [$j]);
}
echo $strSmall;
echo "<br>";

//while
while (isset($str[$i])) {
    $strSmall .= mb_strtolower($str[$i]);
    $i++;
}
echo $strSmall;
echo "<br>";

//do while
$str = 'Hi I am ALex';
$i = 0;
$strSmall = '';
do {
    $strSmall .= mb_strtolower($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strSmall;
echo "<br>";

//foreach
$str = 'Hi I am ALex';
$strSmall = '';
$i = 0;
$j = 0;
$arrStr = [];
while (isset($str[$i])) {
    $arrStr[] = $str[$i];
    $i++;
}
foreach ($arrStr as $key => $value) {
    $strSmall .= mb_strtolower($value);
}
echo $strSmall;
echo "<br>";

/*
 * 5. Дана строка
let str = 'Hi I am ALex'
сделать все буквы большие
 */

$str = 'Hi I am ALex';
echo mb_strtoupper($str);
echo "<br>";
/* If i can't use it, then here :
*/

//for
$strBig = '';
for ($i; isset($str[$i]); $i++) {
    $j = $i;
}
for ($j = 0; $j <= $i; $j++) {
    $strBig .= mb_strtoupper($str[$j]);
}
echo $strBig;
echo "<br>";

//while
while (isset($str[$i])) {
    $strBig .= mb_strtoupper($str[$i]);
    $i++;
}
echo $strBig;
echo "<br>";

//do while
do {
    $strBig .= mb_strtoupper($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strBig;
echo "<br>";

//foreach
$str = 'Hi I am ALex';
$i = 0;
$j = 0;
$strBig = '';
$arrStrBig = [];
while (isset($str[$i])) {
    $arrStrBig[] = $str[$i];
    $i++;
}
foreach ($arrStrBig as $key => $value) {
    $strBig .= mb_strtoupper($value);
}
echo $strBig;
echo "<br>";

/*
 * 6. Дана строка
let str = 'Hi I am ALex'
развернуть ее в обратном направлении
 */

$str = 'Hi I am ALex';

//for
$i = 0;
$j = 0;
$arrLetter = [];
for ($i; $str[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrLetter[] = $str[$j];
}
print_r($arrLetter);
echo "<br>";

//foreach
$str = 'Hi I am ALex';
$i = 0;
$strRevers = [];
while (isset($str[$i])) {
    $strRevers [] = $str[$i];
    $i++;
}
$stringReverse = '';
foreach ($strRevers as $key => $value) {
    $i--;
    $stringReverse .= $strRevers[$i] . "<br>";
}
echo $stringReverse;
echo "<br>";

//while
$i = 0;
$j = 0;
$arrLetter = [];
while (isset($str[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrLetter[] = $str[$j];
    $j--;
}
print_r($arrLetter);
echo "<br>";

// do while
$i = 0;
$j = 0;
$arrLetter = [];
do {
    $j = $i;
    $i++;
} while (isset($str[$i]));
do {
    $arrLetter[] = $str[$j];
    $j--;
} while ($j >= 0);
print_r($arrLetter);
echo "<br>";

/*
 * 7. Дан массив
['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
сделать все буквы с маленькой
 */

//for
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSmall = [];
for ($i = 0; isset($arr[$i]); $i++) {
    $arrSmall [] = mb_strtolower($arr[$i]);
}
print_r($arrSmall);
echo "<br>";

//while
$i = 0;
$arrSmall = [];
while (isset($arr[$i])) {
    $arrSmall[] = mb_strtolower($arr[$i]);
    $i++;
}
print_r($arrSmall);
echo "<br>";

//do while
$i = 0;
$arrSmall = [];
do {
    $arrSmall[] = mb_strtolower($arr[$i]);
    $i++;
} while (isset($arr[$i]));
print_r($arrSmall);
echo "<br>";

//foreach
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSmall = [];
foreach ($arr as $key => $value) {
    $arrSmall[] = mb_strtolower($value);
}
print_r($arrSmall);
echo "<br>";

/*
 * 9. Дано число
let num = 1234678
развернуть ее в обратном направлении
 */

//for
$num = 1234678;
$i = -1;
$numRevers = '';
$num = (string)$num;
for ($i = -1; isset($num[$i]); $i--) {
    $numRevers .= $num[$i];
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo "<br>";

//while
$num = (string)$num;
while (isset($num[$i])) {
    $numRevers .= $num[$i];
    $i--;
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo "<br>";

//do while
$num = (string)$num;
do {
    $numRevers .= $num[$i];
    $i--;
} while (isset($num[$i]));
$numRevers = (int)$numRevers;
echo $numRevers;
echo "<br>";

//foreach
$num = (string)$num;
$i = 0;
while (isset($num[$i])) {
    $numArr[] = $num[$i];
    $i++;
}
$i = -1;
foreach ($numArr as $value) {
    $numRevers .= $numArr[$i];
    $i--;
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo "<br>";

/*
 * 10. Дан массив
[44, 12, 11, 7, 1, 99, 43, 5, 69]
отсортируй его в порядке убывания
 */
//for
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
while (isset($arr[$count])) {
    $count++;
}
for ($i = 0; $i < $count; $i++) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
    }
}
print_r($arr);
echo "<br>";

//while
while (isset($arr[$i])) {
    $count = $i + 1;
    $i++;
}
while ($i < $count) {
    while ($j < $count - 1) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
        $j++;
    }
    $i++;
    if ($j == $count - 1)
        $j = 0;
}
print_r($arr);
echo "<br>";

//do while
$count = 0;
$i = 0;
do {
    $count = $i + 1;
    $i++;
} while (isset($arr[$i]));
$i = 0;
$j = 0;
do {
    $i++;
    if ($j == $count - 1)
        $j = 0;
    do {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
        $j++;
    } while ($j < $count - 1);
} while ($i < $count);
print_r($arr);
echo "<br>";

//foreach
while (isset($arr[$count])) {
    $count++;
}
foreach ($arr as $key => $value) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
    }
}
print_r($arr);
echo "<br>";
?>

